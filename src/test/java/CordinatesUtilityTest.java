import com.lunch.delivery.models.Position;
import com.lunch.delivery.utils.IRoutesCoordinatesUtility;
import com.lunch.delivery.utils.IRoutesCoordinatesUtilityImpl;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class CordinatesUtilityTest {

    static IRoutesCoordinatesUtility routesCoordinatesUtility = new IRoutesCoordinatesUtilityImpl();

    @Test
    public void testEmptyRoutes() {
        List<String> testRoutes = new ArrayList<>();
        List<Position> expectedOutput = routesCoordinatesUtility.getPositionsFromRoutes(testRoutes);
        assertArrayEquals(expectedOutput.toArray() , expectedOutput.toArray());
    }


    /*
    * Individually performed test on one of the routes
    * */
    @Test
    public void testCaseFirstRoute(){
        List<String> testRoutes = new ArrayList<>();
        testRoutes.add("AAAAIAA");
        Position expectedOutput = Position.builder().x(-2L).y(4L).direction('O').build();
        Position positionCalculated = routesCoordinatesUtility.getPositionsFromRoutes(testRoutes).get(0);
        assertEquals(positionCalculated,expectedOutput);
    }


    /*
     * Individually performed test on one of the routes
     * */
    @Test
    public void testCaseSecondRoute(){
        List<String> testRoutes = new ArrayList<>();
        testRoutes.add("DDDAIAD");
        Position expectedOutput = Position.builder().x(1L).y(1L).direction('E').build();
        Position positionCalculated = routesCoordinatesUtility.getPositionsFromRoutes(testRoutes).get(0);
        assertEquals(positionCalculated,expectedOutput);
    }

    /*
     * Individually performed test on one of the routes
     * */
    @Test
    public void testCaseThirdRoute(){
        List<String> testRoutes = new ArrayList<>();
        testRoutes.add("AAIADAD");
        Position expectedOutput = Position.builder().x(-1L).y(3L).direction('E').build();
        Position positionCalculated = routesCoordinatesUtility.getPositionsFromRoutes(testRoutes).get(0);
        assertEquals(positionCalculated,expectedOutput);
    }

    /*
    * The expected output is failing on the first attempt given the following explanation
    * if you are on a cartesian plane , pointing to the North, and go ahead 4 steps , and then you turn to left(Oeste/Occidente)
    * and then go ahead 2 steps you are actually pointing to (Oeste/Occidente) (there wasnt another turn to change the pointing to the North)
    * */
    @Test
    public void testCompleteAsExpectedOnPDF(){
        List<String> testRoutes = new ArrayList<>();
        testRoutes.add("AAAAIAA");
        testRoutes.add("DDDAIAD");
        testRoutes.add("AAIADAD");
        List<Position> positionsExpected = new ArrayList<>();
        positionsExpected.add(Position.builder().x(-2L).y(4L).direction('N').build());
        positionsExpected.add(Position.builder().x(-3L).y(3L).direction('S').build());
        positionsExpected.add(Position.builder().x(-4L).y(2L).direction('E').build()); // Oriente is used like Este on this particular case
        List<Position> positionsCalculated = routesCoordinatesUtility.getPositionsFromRoutes(testRoutes);
        assertArrayEquals(positionsCalculated.toArray(), positionsExpected.toArray());
    }

    @Test
    public void testCompleteCorrectResult(){
        List<String> testRoutes = new ArrayList<>();
        testRoutes.add("AAAAIAA");
        testRoutes.add("DDDAIAD");
        testRoutes.add("AAIADAD");
        List<Position> positionsExpected = new ArrayList<>();
        positionsExpected.add(Position.builder().x(-2L).y(4L).direction('O').build());
        positionsExpected.add(Position.builder().x(-3L).y(3L).direction('N').build());
        positionsExpected.add(Position.builder().x(-4L).y(6L).direction('E').build()); // Este/Oriente
        List<Position> positionsCalculated = routesCoordinatesUtility.getPositionsFromRoutes(testRoutes);
        assertArrayEquals(positionsCalculated.toArray(), positionsExpected.toArray());
    }

}
