package com.lunch.delivery;

import com.lunch.delivery.exception.DronRangeException;
import com.lunch.delivery.models.Dron;
import com.lunch.delivery.models.Position;
import com.lunch.delivery.service.IReportService;
import com.lunch.delivery.service.ReportServiceImpl;
import com.lunch.delivery.utils.DronFactory;
import com.lunch.delivery.utils.IRoutesCoordinatesUtility;
import com.lunch.delivery.utils.IRoutesCoordinatesUtilityImpl;

import java.util.List;

public class Main {

    static DronFactory dronFactory = new DronFactory();
    static IRoutesCoordinatesUtility routesCoordinatesUtility = new IRoutesCoordinatesUtilityImpl();
    static IReportService reportService = new ReportServiceImpl();

    public static void main(String[] args) {
        List<Dron> drones = dronFactory.getDrones(20L);
        drones.stream().forEach(dron -> {
            String dronId = dron.getId().toString().length() == 1 ? "0" + dron.getId().toString() : dron.getId().toString();
            List<String> routes = routesCoordinatesUtility.getRoutesFromFile(String.format("in%s.txt", dronId));
            if (routes.size() > 0) {
                List<Position> positions;
                try {
                    positions = routesCoordinatesUtility.getPositionsFromRoutes(routes);
                    reportService.createReportFile(positions, dronId);
                } catch (DronRangeException dre) {
                    reportService.createReportFailureDron(dronId);
                }
            }
        });

    }
}
