package com.lunch.delivery.exception;

public class DronRangeException extends Exception {
    public DronRangeException(String errorMessage){
        super(errorMessage);
    }
}
