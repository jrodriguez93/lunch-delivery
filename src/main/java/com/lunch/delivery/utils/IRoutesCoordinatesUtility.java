package com.lunch.delivery.utils;

import com.lunch.delivery.exception.DronRangeException;
import com.lunch.delivery.models.Position;

import java.util.List;

public interface IRoutesCoordinatesUtility {
    /*
     * Get the positions based on the route given by the files
     * with the routes specified eith steps or turns.
     * */
    List<Position> getPositionsFromRoutes(List<String> routes) throws DronRangeException;

    /*
    * Get the routes specified on a file and puts into a List
    * */
    List<String> getRoutesFromFile(String path) ;

}
