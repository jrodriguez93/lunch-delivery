package com.lunch.delivery.utils;

import com.lunch.delivery.models.Dron;
import com.lunch.delivery.models.Position;
import com.lunch.delivery.models.enums.DronEnum;

import java.util.ArrayList;
import java.util.List;

public class DronFactory {
    Position initialPosition = Position.builder().x(DronEnum.STARTING_X_POSITION.getValue()).y(DronEnum.STARTING_Y_POSITION.getValue()).direction('N').build();

    public List<Dron> getDrones(Long numberOfDrones){
        List<Dron> drones = new ArrayList<Dron>();
        while(numberOfDrones > 0){
            drones.add(Dron.builder()
                    .id(numberOfDrones)
                    .lowCapacity(DronEnum.MIN_CAPACITY.getValue())
                    .maxCapacity(DronEnum.MIN_CAPACITY.getValue())
                    .position(initialPosition)
                    .rangeOfAction(DronEnum.MAX_RANGE.getValue())
                    .build());
            numberOfDrones--;
        }

        return drones;
    }
}
