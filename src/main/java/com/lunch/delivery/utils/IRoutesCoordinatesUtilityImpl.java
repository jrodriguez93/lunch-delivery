package com.lunch.delivery.utils;

import com.lunch.delivery.exception.DronRangeException;
import com.lunch.delivery.models.Position;
import com.lunch.delivery.models.enums.DronEnum;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IRoutesCoordinatesUtilityImpl implements IRoutesCoordinatesUtility {

    @Override
    public List<Position> getPositionsFromRoutes(List<String> routes) throws DronRangeException{
        List<Position> positions = new ArrayList<>();
        Position updatedposition = Position.builder().x(DronEnum.STARTING_X_POSITION.getValue()).y(DronEnum.STARTING_Y_POSITION.getValue()).direction('N').build();

        for (String route : routes) {
            Arrays.stream(route.split("(?!^)")).forEach(step -> {
                if (step.equals("D") || step.equals("I")) {
                    updatedposition.setDirection(getCalculatedDirection(updatedposition.getDirection(), step));
                } else if (step.equals("A")) {
                    updatedposition.setX(getCalculatedPositionX(updatedposition));
                    updatedposition.setY(getCalculatedPositionY(updatedposition));
                }
            });
            if(updatedposition.getX() >= 10 || updatedposition.getX() <= -10 || updatedposition.getY() >= 10 || updatedposition.getY() <= -10){
                throw new DronRangeException("The dron is far away");
            }
            positions.add(Position.builder().x(updatedposition.getX()).y(updatedposition.getY()).direction(updatedposition.getDirection()).build());
        }
        return positions;
    }


    private Long getCalculatedPositionX(Position updatedPosition) {
        if (updatedPosition.getDirection() == 'E') {
            return updatedPosition.getX() + 1;
        } else if (updatedPosition.getDirection() == 'O') {
            return updatedPosition.getX() - 1;
        }
        return updatedPosition.getX();
    }

    private Long getCalculatedPositionY(Position updatedPosition) {
        if (updatedPosition.getDirection() == 'S') {
            return updatedPosition.getY() - 1;
        } else if (updatedPosition.getDirection() == 'N') {
            return updatedPosition.getY() + 1;
        }
        return updatedPosition.getY();
    }

    /*
     * Something to mark here is that the position is given related with the cartesian plane
     * is not related with the actual view of the Drone
     *
     * Oriente es Este
     * */
    private char getCalculatedDirection(char currentDirection, String step) {
        if (currentDirection == 'N') {
            return step.equals("D") ? 'E' : step.equals("I") ? 'O' : 'N';
        } else if (currentDirection == 'E') {
            return step.equals("D") ? 'S' : step.equals("I") ? 'N' : 'E';
        } else if (currentDirection == 'S') {
            return step.equals("D") ? 'E' : step.equals("I") ? 'O' : 'S';
        } else if (currentDirection == 'O') {
            return step.equals("D") ? 'N' : step.equals("I") ? 'S' : 'O';
        } else {
            return 'N';
        }
    }

    @Override
    public List<String> getRoutesFromFile(String path) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        List<String> routes = new ArrayList<String>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(classLoader.getResource(path).getFile()));
            String str;
            while ((str = in.readLine()) != null) {
                routes.add(str);
            }
        } catch (Exception e) {
            return routes;
        }
        return routes;
    }

}
