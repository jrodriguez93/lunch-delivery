package com.lunch.delivery.models.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum DronEnum {
    STARTING_X_POSITION(0L),
    STARTING_Y_POSITION(0L),
    MIN_CAPACITY(3L),
    MAX_CAPACITY(3L),
    MAX_RANGE(10L);


    private Long value;

}
