package com.lunch.delivery.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Dron {
    private Long id;
    private Position position;
    private Long maxCapacity;
    private Long lowCapacity;
    private Long rangeOfAction;
}
