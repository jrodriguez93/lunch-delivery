package com.lunch.delivery.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class Position {
    private Long x;
    private Long y;
    private char direction;
}
