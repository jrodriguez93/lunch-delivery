package com.lunch.delivery.service;

import com.lunch.delivery.models.Position;

import java.util.List;

public interface IReportService {
    void createReportFile(List<Position> positions, String droneId);
    void createReportFailureDron(String droneId);
}
