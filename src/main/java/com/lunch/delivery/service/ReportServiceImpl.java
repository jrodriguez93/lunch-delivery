package com.lunch.delivery.service;

import com.lunch.delivery.models.Position;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class ReportServiceImpl implements IReportService {

    @Override
    public void createReportFile(List<Position> positions, String droneId) {
        try {
            if(positions.size() > 0){
                File file = new File(String.format("%s.txt", this.getClass().getClassLoader().getResource(".").getPath() +"out"+ droneId));
                FileWriter writer = new FileWriter(file);
                writer.write(String.format("== Reporte de entregas ==\n\n"));
                for (Position position : positions) {
                    writer.write(String.format("(%d, %d) dirección %s\n", position.getX(), position.getY(), position.getDirection() == 'N' ? "Norte" : position.getDirection() == 'S' ? "Sur" : position.getDirection() == 'E' ? "Oriente" : "Occidente"));
                }
                writer.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createReportFailureDron(String droneId) {
        try {
            File file = new File(String.format("%s.txt", this.getClass().getClassLoader().getResource(".").getPath() +"out"+ droneId));
            FileWriter writer = new FileWriter(file);
            writer.write(String.format("== The Drone is missing ==\n\n stop the operation"));
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
